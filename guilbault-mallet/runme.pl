#!/usr/bin/perl
use strict;
use warnings;
use autodie;

sub run{
print "running othello\n";
`java com.forbun.othello.Othello`;
}

sub main{unless(-e 'com/forbun/othello/Othello.class'){
	print "compiling othello...\n";
	`make all`;
}
run;
}

main;
