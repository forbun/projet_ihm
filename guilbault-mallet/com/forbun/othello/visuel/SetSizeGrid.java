package com.forbun.othello.visuel;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class SetSizeGrid extends JDialog {
    public enum DialogStatus {
        ACCEPTED,
        REJECTED
    }
    private ActionListener al = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == confirmBut) {
                accept();
            }
            if(e.getSource() == cancelBut) {
                reject();
            }
        }
    };

    private JSpinner rowsSpin;
    private JSpinner colsSpin;
    private JButton confirmBut;
    private JButton cancelBut;
    private int basecol;
    private int baserow;
    private DialogStatus ds;

    public SetSizeGrid(int rows, int cols) {
        super();

        this.ds = DialogStatus.REJECTED;
        this.basecol = cols;
        this.baserow = rows;
        this.rowsSpin = new JSpinner(new SpinnerNumberModel(rows,1,10,1));
        this.rowsSpin.setMaximumSize(new Dimension(80,25));
        this.rowsSpin.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.colsSpin = new JSpinner(new SpinnerNumberModel(cols,1,10,1));
        this.colsSpin.setMaximumSize(new Dimension(80,25));
        this.colsSpin.setAlignmentX(Component.CENTER_ALIGNMENT);

        this.setResizable(false);
        this.setModal(true);

        this.confirmBut = new JButton("OK");
        this.confirmBut.addActionListener(this.al);
        this.cancelBut = new JButton("Cancel");
        this.cancelBut.addActionListener(this.al);

        JLabel rowsLab = new JLabel("Lignes :");
        rowsLab.setAlignmentX(Component.CENTER_ALIGNMENT);
        JLabel colsLab = new JLabel("Colonnes :");
        colsLab.setAlignmentX(Component.CENTER_ALIGNMENT);

        Box bRows = Box.createVerticalBox();
        bRows.add(rowsLab);
        bRows.add(this.rowsSpin);
        Box bCols = Box.createVerticalBox();
        bCols.add(colsLab);
        bCols.add(this.colsSpin);

        Box bline1 = Box.createHorizontalBox();
        bline1.add(bRows);
        bline1.add(Box.createRigidArea(new Dimension(15,15)));
        bline1.add(bCols);
        Box bline2 = Box.createHorizontalBox();
        bline2.add(Box.createRigidArea(new Dimension(15,15)));
        bline2.add(confirmBut);
        bline2.add(Box.createRigidArea(new Dimension(15,15)));
        bline2.add(cancelBut);
        bline2.add(Box.createRigidArea(new Dimension(15,15)));

        Box bAll = Box.createVerticalBox();
        bAll.add(Box.createRigidArea(new Dimension(15,15)));
        bAll.add(bline1);
        bAll.add(Box.createRigidArea(new Dimension(15,15)));
        bAll.add(bline2);
        bAll.add(Box.createRigidArea(new Dimension(15,15)));

        this.add(bAll);
    }

    private void accept() {
        this.ds = DialogStatus.ACCEPTED;
        this.dispose();
    }

    private void reject() {
        this.ds = DialogStatus.REJECTED;
        this.colsSpin.setValue(basecol);
        this.rowsSpin.setValue(baserow);
        this.dispose();
    }

    public DialogStatus getStatus() {
        return this.ds;
    }

    public int getRows() {
        return (int) this.rowsSpin.getValue();
    }

    public int getCols() {
        return (int) this.colsSpin.getValue();
    }

    public static void main(String[] args) {
        final SetSizeGrid frame = new SetSizeGrid(4,4);
        frame.setDefaultCloseOperation(SetSizeGrid.DISPOSE_ON_CLOSE);
        frame.setTitle("SetSizeGrid");

        // Print values
        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent event) {
            }

            @Override
            public void windowClosing(WindowEvent event) {
            }

            @Override
            public void windowClosed(WindowEvent event) {
                if(frame.getStatus() == DialogStatus.ACCEPTED) {
                    System.out.println("OK / Rows:" + frame.getRows() +
                            " Cols:" + frame.getCols());
                } else {
                    System.out.println("Cancel");
                }
            }

            @Override
            public void windowIconified(WindowEvent event) {
            }

            @Override
            public void windowDeiconified(WindowEvent event) {
            }

            @Override
            public void windowActivated(WindowEvent event) {
            }

            @Override
            public void windowDeactivated(WindowEvent event) {
            }
        });

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                //Display the window.
                frame.pack();
                frame.setVisible(true);
            }
        });
    }
}
