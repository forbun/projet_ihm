package com.forbun.othello.logique;

public enum Etat {
    NOIR,BLANC;

    public Etat flip(){
        if(this == BLANC){
            return NOIR;
        }
        else{
            return BLANC;
        }
    }
}
