package com.forbun.othello;

import com.forbun.othello.logique.DamierLogique;
import com.forbun.othello.visuel.Damier;
import com.forbun.othello.visuel.SetSizeGrid;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class Othello extends JFrame {
    private ActionListener al = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == newGameBut) {
                newGame();
            }
            if(e.getSource() == changeSizeBut) {
                changeGridSize();
            }
            if(e.getSource() == quitBut) {
                System.exit(0);
            }
            if(e.getSource() instanceof DamierLogique) {
                DamierLogique d = (DamierLogique) e.getSource();
                updateNbCoups(d.getCoups());
                if(d.check_victory()){
                    if(d.optimal()) {
                        JOptionPane.showMessageDialog(null, "Jeu termin� !\n" +
                                "Nombre de coups : " + d.getCoups());
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "Jeu termin�, " +
                                "mais pas avec un nombre minimum de coups !\n" +
                                "Nombre de coups : " + d.getCoups());
                    }
                }
            }
        }
    };

    private WindowListener wl = new WindowListener() {
        @Override
        public void windowOpened(WindowEvent event) {
        }

        @Override
        public void windowClosing(WindowEvent event) {
        }

        @Override
        public void windowClosed(WindowEvent event) {
            if(event.getSource() instanceof SetSizeGrid) {
                SetSizeGrid ssg = (SetSizeGrid) event.getSource();
                resizeDamier(ssg.getRows(), ssg.getCols());
            }
        }

        @Override
        public void windowIconified(WindowEvent event) {
        }

        @Override
        public void windowDeiconified(WindowEvent event) {
        }

        @Override
        public void windowActivated(WindowEvent event) {
        }

        @Override
        public void windowDeactivated(WindowEvent event) {
        }
    };

    private Damier damier;
    private JButton quitBut;
    private JButton newGameBut;
    private JButton changeSizeBut;
    private JTextArea stats;
    private Box bLeft;

    public Othello(int rows, int cols) {
        super();

        this.damier = new Damier(rows,cols);
        this.damier.addActionListener(this.al);
        this.quitBut = new JButton("Quitter");
        this.quitBut.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.quitBut.addActionListener(this.al);
        this.newGameBut = new JButton("Nouvelle Partie");
        this.newGameBut.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.newGameBut.addActionListener(this.al);
        this.changeSizeBut = new JButton("Changer la grille");
        this.changeSizeBut.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.changeSizeBut.addActionListener(this.al);
        this.stats = new JTextArea(10,30);
        this.stats.setPreferredSize(new Dimension(300,100));
        this.stats.setMinimumSize(new Dimension(300,100));
        this.stats.setMaximumSize(new Dimension(300,100));
        this.stats.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.stats.setEditable(false);

        this.updateNbCoups(0);

        this.bLeft = Box.createVerticalBox();
        this.bLeft.add(this.damier);

        Box bRight = Box.createVerticalBox();
        bRight.add(Box.createGlue());
        bRight.add(Box.createRigidArea(new Dimension(10,10)));
        bRight.add(this.stats);
        bRight.add(Box.createRigidArea(new Dimension(20,20)));
        bRight.add(this.newGameBut);
        bRight.add(Box.createRigidArea(new Dimension(10,10)));
        bRight.add(this.changeSizeBut);
        bRight.add(Box.createRigidArea(new Dimension(10,10)));
        bRight.add(this.quitBut);
        bRight.add(Box.createRigidArea(new Dimension(10,10)));
        bRight.add(Box.createGlue());

        Box bAll = Box.createHorizontalBox();
        bAll.add(Box.createGlue());
        bAll.add(Box.createRigidArea(new Dimension(15,15)));
        bAll.add(this.bLeft);
        bAll.add(Box.createRigidArea(new Dimension(15,15)));
        bAll.add(bRight);
        bAll.add(Box.createRigidArea(new Dimension(15,15)));
        bAll.add(Box.createGlue());

        this.add(bAll);
    }

    private void newGame() {
        int rows = this.damier.getNbrRows();
        int cols = this.damier.getNbrCols();

        this.bLeft.removeAll();
        this.damier = new Damier(rows,cols);
        this.damier.addActionListener(this.al);
        this.bLeft.add(this.damier);
        this.updateNbCoups(0);

        this.pack();
    }

    private void changeGridSize() {
        final SetSizeGrid ssg = new SetSizeGrid(this.damier.getNbrRows(),
                this.damier.getNbrCols());
        ssg.addWindowListener(this.wl);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ssg.pack();
                ssg.setVisible(true);
            }
        });
    }

    private void resizeDamier(int rows, int cols) {
        this.bLeft.removeAll();
        this.damier = new Damier(rows,cols);
        this.damier.addActionListener(this.al);
        this.bLeft.add(this.damier);
        this.updateNbCoups(0);

        this.pack();
    }

    private void updateNbCoups(int nbCoups) {
        this.stats.setText("Nombre de coups jou�s : " + nbCoups);
    }

    public static void main(String[] args) {
        int rows;
        int cols;
        if(args.length < 2) {
            rows = 4;
            cols = 3;
        } else {
            rows = Integer.valueOf(args[0]);
            cols = Integer.valueOf(args[1]);
        }
        final Othello frame = new Othello(rows,cols);
        frame.setDefaultCloseOperation(Othello.EXIT_ON_CLOSE);
        frame.setTitle("Othello");

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            //Display the window.
            frame.pack();
            frame.setVisible(true);
            }
        });
    }
}
