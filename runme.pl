#!/usr/bin/perl
use strict;
use warnings;
use autodie;

sub run{
print "going to the right directory\n";
chdir 'othello/src';
print "running othello\n";
`java com.forbun.othello.Othello`;
}

sub main{unless(-e 'othello/src/com/forbun/othello/Othello.class'){
	print "compiling othello...\n";
	`make all`;
}
run;
}

main;
