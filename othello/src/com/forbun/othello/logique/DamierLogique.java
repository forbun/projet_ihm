package com.forbun.othello.logique;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class DamierLogique{
    Pion[][] pions;
    int x;
    int y;
    private int coups;
    private Coups[][] stats;
    private ArrayList<ActionListener> observers;

    public DamierLogique(int x, int y){
        this.pions = new Pion[x][y];
        this.x = x;
        this.y = y;
        this.observers = new ArrayList<>();
        this.stats = new Coups[x][y];
        for(int i = 0; i < x; i++){
            for(int j = 0; j < y; j++){
                this.stats[i][j] = Coups.NONE;
            }
        }
    }

    public boolean check_victory(){
        Etat pzero = this.pions[0][0].etat;
        if(pzero == Etat.BLANC && this.coups % 2 == 0){
            return false;
        }
        for(int i = 0; i < this.x;i++){
            for(int j = 0; j < this.y;j++){
               if(this.pions[i][j].etat != pzero) {
                   return false;
               }
            }
        }
        return true;
    }

    public boolean optimal(){
        for(int i = 0; i < this.x; i++){
            for(int j = 0; j < this.y;j++){
                if(this.stats[i][j] == Coups.MANY){
                    return false;
                }
            }
        }
        return true;
    }

    public void flip(Pion monpion, boolean incrCps){
        int basex = monpion.x;
        int basey = monpion.y;
        if(incrCps) {
            this.coups++;

            if(this.stats[basex][basey] == Coups.NONE){
                this.stats[basex][basey] = Coups.ONE;
            }
            else if(this.stats[basex][basey]==Coups.ONE){
                this.stats[basex][basey] = Coups.MANY;
            }
        }
        flipLeft(basex, basey);
        flipVSame(basex, basey);
        flipRight(basex, basey);
        if(incrCps) {
            this.updateActionListeners();
        }
    }

    private void flipLeft(int basex, int basey){
        this.flipNeighbor(basex-1,basey+1);
        this.flipNeighbor(basex-1,basey);
        this.flipNeighbor(basex-1,basey-1);
    }

    private void flipVSame(int basex, int basey){
        this.flipNeighbor(basex,basey-1);
        this.flipNeighbor(basex,basey+1);
    }

    private void flipRight(int basex, int basey){
        this.flipNeighbor(basex+1,basey+1);
        this.flipNeighbor(basex+1,basey);
        this.flipNeighbor(basex+1,basey-1);
    }

    private void flipNeighbor(int x, int y){
        Etat curstate;
        if(x >= 0 && x < this.x && y >= 0 && y <this.y) {
            Pion neighbor = this.getPion(x, y);
            curstate = neighbor.etat;
            neighbor.etat = curstate.flip();
            neighbor.visuel.repaint();
        }
    }
    private Pion getPion(int x, int y){
        return this.pions[x][y];
    }

    public int getCoups() {
        return coups;
    }

    public void addActionListener(ActionListener obs) {
        this.observers.add(obs);
    }

    public void removeActionListener(ActionListener obs) {
        this.observers.remove(obs);
    }

    public void removeActionListeners() {
        this.observers.clear();
    }

    protected void updateActionListeners() {
        ActionEvent e = new ActionEvent(this, ActionEvent.ACTION_FIRST, "flip");
        for(ActionListener obs : this.observers) {
            obs.actionPerformed(e);
        }
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }
}