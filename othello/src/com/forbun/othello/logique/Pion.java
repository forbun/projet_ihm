package com.forbun.othello.logique;

public class Pion {
    private DamierLogique damier;
    public Etat etat;
    public int x;
    public int y;
    public com.forbun.othello.visuel.Pion visuel;

    public Pion(DamierLogique mondamier, Etat etat, int x, int y,
                com.forbun.othello.visuel.Pion monpion){
        this.damier = mondamier;
        this.etat = etat;
        this.x = x;
        this.y = y;
        visuel = monpion;

        this.damier.pions[x][y] = this;
    }

    public void flipMe(boolean incrCps){
        this.damier.flip(this, incrCps);
    }
}
