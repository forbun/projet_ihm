package com.forbun.othello.visuel;

import com.forbun.othello.logique.DamierLogique;
import com.forbun.othello.logique.Etat;

import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.ActionListener;

public class Damier extends JPanel {
    DamierLogique damierLogique;

    public Damier(int lignes, int colonnes){
        super(new GridLayout(lignes,colonnes));
        damierLogique = new DamierLogique(lignes,colonnes);
        for(int i = 0; i < lignes; i++){
            for(int j = 0; j<colonnes;j++){
                this.add(new Pion(damierLogique,Etat.BLANC,i,j));
            }
        }
    }

    public void addActionListener(ActionListener listener) {
        this.damierLogique.addActionListener(listener);
    }

    public int getNbrRows() {
        return this.damierLogique.getX();
    }

    public int getNbrCols() {
        return this.damierLogique.getY();
    }
}
