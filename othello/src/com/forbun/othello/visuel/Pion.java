package com.forbun.othello.visuel;

import com.forbun.othello.logique.DamierLogique;
import com.forbun.othello.logique.Etat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Pion extends JComponent implements MouseListener{
    com.forbun.othello.logique.Pion pionLogique;
    public Pion(DamierLogique damier,Etat etat, int x, int y){
        super();
        this.pionLogique = new com.forbun.othello.logique.Pion(
                damier,etat,x,y,this);
        this.setPreferredSize(new Dimension(60,60));
        this.addMouseListener(this);
        this.repaint();
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        g.drawRect(0,0,this.getWidth()-1,this.getHeight()-1);
        if(this.pionLogique.etat == Etat.BLANC){
            g.drawOval((this.getWidth()*2)/10, (this.getHeight()*2)/10,
                    (this.getWidth()*6)/10, (this.getHeight()*6)/10);
        }
        else{
            g.fillOval((this.getWidth()*2)/10, (this.getHeight()*2)/10,
                    (this.getWidth()*6)/10, (this.getHeight()*6)/10);
        }
     }

    private double ellipseEquationPion(int x, int y) {
        int w = this.getWidth();
        int h = this.getHeight();

        float c1 = (x - 0.5f * w) / (0.3f * w);
        float c2 = (y - 0.5f * h) / (0.3f * h);

        return (Math.pow(c1,2) + Math.pow(c2,2));
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if(mouseEvent.getButton() == MouseEvent.BUTTON1) {
            int x = mouseEvent.getX();
            int y = mouseEvent.getY();
            if (ellipseEquationPion(x, y) <= 1) {
                com.forbun.othello.visuel.Pion curpion =
                        (Pion) mouseEvent.getSource();
                curpion.pionLogique.flipMe(true);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        if(mouseEvent.getButton() == MouseEvent.BUTTON3) {
            int x = mouseEvent.getX();
            int y = mouseEvent.getY();
            if(ellipseEquationPion(x,y) <= 1) {
                com.forbun.othello.visuel.Pion curpion =
                        (Pion) mouseEvent.getSource();
                curpion.pionLogique.flipMe(false);
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        if(mouseEvent.getButton() == MouseEvent.BUTTON3) {
            int x = mouseEvent.getX();
            int y = mouseEvent.getY();
            if(ellipseEquationPion(x,y) <= 1) {
                com.forbun.othello.visuel.Pion curpion =
                        (Pion) mouseEvent.getSource();
                curpion.pionLogique.flipMe(false);
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
